/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pennyant;

import java.util.Scanner;


/**
 *
 * @author l.vanlaake
 */
public class PennyAnt {
    final static double CONST = 123456789.123456789;  
    // ^ eulers constante is een wiskundige constante en een irrationeel getal
    static int exponent = 1; 
    // exponent gaat elke keer omhoog om te garanderen dat elke iteratie een willekeurig nummer geeft
    static Scanner sc = new Scanner(System.in);
    // de drie arrays met de gevonden waarden en de voorspellingen van Bob en Allice waarin 'true' staat voor kop en false voor munt
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int count = 0;

        int max = getMax();
        boolean[] results = new boolean[max];
        boolean[] allice = new boolean[max];
        allice = getGuessArray("Allice", allice);
        boolean[] bob = new boolean[max];
        bob = getGuessArray("Bob", bob);
       
        while (true) { // loopt door tot Bob of Allice winnen
            count ++; // telt gegooide muntjes
            results = doorschuif(results); // schuift hem door
        
            if (sameArray(results, allice) && count >= 3) {
                System.out.println("Allice has won in " + count + " turns.");
                break;
                // resultaten stemmen overeen met de voorspelling van Allice, zij wint en spel stopt 
            } else if (sameArray(results, bob)  && count >= 3) {
                System.out.println("Bob has won in " + count + " turns.");
                break;
                 // resultaten stemmen overeen met de voorspelling van Bob, hij wint en spel stopt 
        }   
        }
        
    }
    
    private static boolean[] doorschuif (boolean[] arr) {
        if (arr.length <= 1) {
            return arr;
        }
        for (int i = 0; i<arr.length; i++) {
            if (i == arr.length-1) {
                arr[i] = isKop();
            } else {
            arr[i] = arr[i+1];
            }
            
        }
        return arr;
    }
    
    private static boolean sameArray(boolean[] arr1, boolean[] arr2) {
 // functie om te controleren of de array van resultaten overeenkomt met een array van voorspellingen
        for (int i = 0; i <arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
 // controleert voor elke boolean in de resultaten-array of die gelijk is aan de boolean in de voorspellingen-array
 // als er 1 boolean niet overeenkomt returnt de functie 'false'
        }
        return true;
 // als alle booleans overeenkomen returnt de functie 'true'
    }
    
    
    
    private static boolean isKop() {
        
        double number = CONST;
    // neemt Eulers constante als basis
        long time = System.nanoTime()%10;
    // neemt het laatste cijfer van het aantal nano-seconden van dit moment
        number *= exponent;
    // vermenigvuldigt nummer met exponent om vaste posities van getallen te husselen
        number *= time;
    //  vermenigvuldiging van nummer met huidige nanoseconde om te voorkomen dat de reeks hetzelfde blijft
        exponent %= 20;
        exponent += time;
    //  exponent verhoogt per iteratie om te voorkomen dat de functie zelfde uitkomst geeft in dezelfde nanoseconde
    //  exponent is maximaal 20 om onnodig grote berekeningen te voorkomen
    //  bij het opnieuw starten van het programma
        int rand = (int) number % 10;
    //  pakt het laatste hele getal van het gehusselde nummer
        if (rand % 10 < 5) return true;
    //  50% kans dat dit lager is dan 5, 50% kans dat het hoger is dan 5
        else return false;
        
    }
    
    private static boolean isKop2() {
        
     double rand = Math.random()*10;
        if (rand % 10 < 5) return true;
    //  50% kans dat dit lager is dan 5, 50% kans dat het hoger is dan 5
        else return false;
        
    }
    
     private static int getMax () {
       int i = getMaxOnce();
       while (i < 0) {
           i = getMaxOnce();
       }
       return i;
       // gaat door tot er daadwerkelijk een nummer is ingevoerd
    }
    
    private static int getMaxOnce () {

    System.out.println("Voer in hoe veel keer geworpen moet worden");
    String s = sc.next();
    try {
        int i = Integer.parseInt(s);
        return i;
    } catch (Exception e) {
        System.out.println("Dit was geen integer");
        return -1;
    }
        // -1 voor error
    }
    
    
    private static String getGuess(String name, int beurt) {
        beurt ++;
        System.out.println(name + ", voer een gok in voor beurt " + beurt);
        String str = sc.next().toLowerCase();
        if (str.equals("kop")) {
            return "true";
        } else if (str.equals("munt")) {
            return "false";
        } else {
            System.out.println("Vul a.u.b. \'kop\' of \'munt\' in.");
            return "";
        }
        // maakt 'true' of 'false'
    }
    
    private static boolean[] getGuessArray (String name, boolean[] arr) {
        
         for (int i = 0; i < arr.length; i++) {
            String str = getGuess(name, i);
            while (str.length() == 0) {
                str = getGuess(name, i);
            }
            arr[i] = Boolean.getBoolean(str);
        }
        return arr;
        // vult de array met gokken vanuit user input
    }
    
    
    
}
