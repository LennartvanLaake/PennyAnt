/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Groetjes Mike.
 */
package schrikkelpriem;

import javax.swing.JOptionPane;

/**
 *
 * @author Mike Smit
 */

public class SchrikkelPriem {
    public static void main (String[] a) {
        String s = "";
        int i = 1;
        while (i > 0) {
            try {
                s = JOptionPane.showInputDialog("Geef een getal (0 om te stoppen)");
                i = Integer.parseInt (s);
                if (i != 0) {
                    if (test(i)) {
                        System.out.println ("De waarde " + i + " is een SchrikkelPriem.");
                    } else {
                        System.out.println ("De waarde " + i + " is geen SchrikkelPriem.");
                    }
                }
            } catch (NumberFormatException e) { 
                System.out.println ("Helaas '" + s + "' is geen integer.");
            }
        }
    }
    
    public static boolean test (int i) {
        throw new UnsupportedOperationException("Method test not (yet) supported");
    }
}

